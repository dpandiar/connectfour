#include <iostream>

#include "Board.h"
#include "Player.h"

using std::cout;
using std::endl;
using std::string;

#define GAME_COLS 7 
#define GAME_ROWS 6
#define WINNING_MOVES 4
	
string color_names[2] = {"RED", "YELLOW"}; //used in printing output 

int test_row_win ();
int test_col_win ();
int test_left_diagonal_win ();
int test_right_diagonal_win ();

int main () {
	
	test_row_win();
	cout << "+++++++++++++++++++++++++++++++++++++++" << endl << endl;
	
	test_col_win();
	cout << "+++++++++++++++++++++++++++++++++++++++" << endl << endl;
	
	test_left_diagonal_win();
	cout << "+++++++++++++++++++++++++++++++++++++++" << endl << endl;
	
	test_right_diagonal_win();
	cout << "+++++++++++++++++++++++++++++++++++++++" << endl << endl;

	return 1;
}

int test_row_win () {
	Board board(GAME_ROWS, GAME_COLS, WINNING_MOVES);

	CoinColor player1_color, player2_color;

	player1_color = RED;
	player2_color = YELLOW;
	Player player1(&board, player1_color);
	Player player2(&board, player2_color);

	int turns = 1;
		
	int player1_start_col = 0;
	int player2_start_col = (GAME_COLS/2) + 1;

	while(!board.isFull()) {
		
		cout << "Iteration: " << turns << "\t";

		if(player1_start_col > GAME_COLS-1){
			player1_start_col = 0;
		}
		if(player2_start_col > GAME_COLS-1) {
			player2_start_col = (GAME_COLS/2) + 1;
		}

		if(turns%2) {
			cout << color_names[player1.get_coincolor()-1] << " player move" << endl;
			player1.move(player1_start_col++);
		} else {
			cout << color_names[player2.get_coincolor()-1] << " player move" << endl;
			player2.move(player2_start_col++);
		}
	
		//display the board with moves so far.
		board.draw();

		if(turns%2) {	
			if(board.isWinner(player1.get_coincolor())) {
					cout << color_names[player1.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		} else {
			if(board.isWinner(player2.get_coincolor())) {
					cout << color_names[player2.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		}
		
		turns++;
	}

	return 1;
}

int test_col_win () {
	Board board(GAME_ROWS, GAME_COLS, WINNING_MOVES);

	CoinColor player1_color, player2_color;

	player1_color = RED;
	player2_color = YELLOW;
	Player player1(&board, player1_color);
	Player player2(&board, player2_color);

	int turns = 1;
		
	int player1_start_col = 0, player2_start_col = 1;

	while(!board.isFull()) {
		
		cout << "Iteration: " << turns << "\t";

		if(turns%2) {
			cout << color_names[player1.get_coincolor()-1] << " player move" << endl;
			player1.move(player1_start_col);
		} else {
			cout << color_names[player2.get_coincolor()-1] << " player move" << endl;
			player2.move(player2_start_col);
		}
	
		//display the board with moves so far.
		board.draw();

		if(turns%2) {	
			if(board.isWinner(player1.get_coincolor())) {
					cout << color_names[player1.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		} else {
			if(board.isWinner(player2.get_coincolor())) {
					cout << color_names[player2.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		}
		
		turns++;
	}

	return 1;
}

int test_left_diagonal_win () {
	Board board(GAME_ROWS, GAME_COLS, WINNING_MOVES);

	CoinColor player1_color, player2_color;

	player1_color = RED;
	player2_color = YELLOW;
	Player player1(&board, player1_color);
	Player player2(&board, player2_color);

	int turns = 1;
		
	int player1_start_col = GAME_COLS-2; 
	int player2_start_col = GAME_COLS-1;
	int player1_round = 0, player2_round = 0;	

	while(!board.isFull()) {
		
		cout << "Iteration: " << turns << "\t";

		if(player1_start_col < GAME_COLS/2){
			player1_round++;
			if(player1_round % 2) 
				player1_start_col = GAME_COLS-1;
			else 
				player1_start_col = GAME_COLS-2;
		}
		if(player2_start_col < GAME_COLS/2) {
			player2_round++;
			if(player2_round % 2) 
				player2_start_col = GAME_COLS-2;
			else 
				player2_start_col = GAME_COLS-1;
		}

		if(turns%2) {
			cout << color_names[player1.get_coincolor()-1] << " player move" << endl;
			player1.move(player1_start_col);
			player1_start_col -= 2;	
		} else {
			cout << color_names[player2.get_coincolor()-1] << " player move" << endl;
			player2.move(player2_start_col);
			player2_start_col -= 2;	
		}
	
		//display the board with moves so far.
		board.draw();

		if(turns%2) {	
			if(board.isWinner(player1.get_coincolor())) {
					cout << color_names[player1.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		} else {
			if(board.isWinner(player2.get_coincolor())) {
					cout << color_names[player2.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		}
		
		turns++;
	}

	return 1;
}

int test_right_diagonal_win () {
	Board board(GAME_ROWS, GAME_COLS, WINNING_MOVES);

	CoinColor player1_color, player2_color;

	player1_color = RED;
	player2_color = YELLOW;
	Player player1(&board, player1_color);
	Player player2(&board, player2_color);

	int turns = 1;
		
	int player1_start_col = GAME_COLS/2; 
	int player2_start_col = (GAME_COLS/2) + 1;
	int player2_round = 0, player1_round = 0;	

	while(!board.isFull()) {
		
		cout << "Iteration: " << turns << "\t";

		if(player1_start_col > GAME_COLS-1){
			player1_round++;
			if(player1_round % 2) 
				player1_start_col = (GAME_COLS/2) + 1 ;
			else 
				player1_start_col = (GAME_COLS/2);
		}
		if(player2_start_col > GAME_COLS-1) {
			player2_round++;
			if(player2_round % 2) 
				player2_start_col = (GAME_COLS/2);
			else 
				player2_start_col = (GAME_COLS/2) + 1;
		}

		if(turns%2) {
			cout << color_names[player1.get_coincolor()-1] << " player move" << endl;
			player1.move(player1_start_col);
			player1_start_col += 2;	
		} else {
			cout << color_names[player2.get_coincolor()-1] << " player move" << endl;
			player2.move(player2_start_col);
			player2_start_col += 2;	
		}
	
		//display the board with moves so far.
		board.draw();

		if(turns%2) {	
			if(board.isWinner(player1.get_coincolor())) {
					cout << color_names[player1.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		} else {
			if(board.isWinner(player2.get_coincolor())) {
					cout << color_names[player2.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		}
		
		turns++;
	}

	return 1;
}
