#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Board.h"
#include "Player.h"

using std::cout;
using std::endl;
using std::string;

#define GAME_COLS 7 
#define GAME_ROWS 6
#define WINNING_MOVES 4

int main () {
	string color_names[2] = {"RED", "YELLOW"}; //used in printing output 
	
	Board board(GAME_ROWS, GAME_COLS, WINNING_MOVES);

	srand(time(0));
	
	CoinColor player1_color, player2_color;

	//Coin toss at beginning to determine who gets what color
	int toss = rand() % 2;
	if(toss) {
		player1_color = RED;
		player2_color = YELLOW;
	} else {
		player1_color = YELLOW;
		player2_color = RED;
	}
	
	Player player1(&board, player1_color);
	Player player2(&board, player2_color);

	board.draw();
	
	int turns = 1;

	//Play until either (1) the board is full or (2) a winning move was made.
	while(!board.isFull()) {
		
		//pick random column for a player move.		
		int rand_col = rand() % GAME_COLS; 
	
		//If that column is full with coins, try again.
		if(board.isColFull(rand_col)) {
			continue;
		}

		cout << "Iteration: " << turns << "\t";

		if(turns%2) {
			cout << color_names[player1.get_coincolor()-1] << " player move" << endl;
			player1.move(rand_col);
		} else {
			cout << color_names[player2.get_coincolor()-1] << " player move" << endl;
			player2.move(rand_col);
		}
	
		//display the board with moves so far.
		board.draw();

		if(turns%2) {	
			if(board.isWinner(player1.get_coincolor())) {
					cout << color_names[player1.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		} else {
			if(board.isWinner(player2.get_coincolor())) {
					cout << color_names[player2.get_coincolor()-1] << " PLAYER WINS!" << endl;
					return 1;
			}
		}
		
		turns++;
	}

	//No more moves to make and nobody had a winning move.
	cout << "TIED GAME!" << endl;
	
	return 1;
}
