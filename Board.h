#ifndef BOARD_H
#define BOARD_H

#define WINNING_MOVES_DEFAULT 4

enum CoinColor {RED = 1, YELLOW};

class Board {

 public:
	Board(int num_rows, int num_cols, int num_win_moves = WINNING_MOVES_DEFAULT); 
	~Board();
	
	void draw() const; //draws the graphical representation of current Board state.
	bool isFull() const; //tells if the board is filled.
	bool isColFull(int column) const; //tells if a column is filled.
	void make_move(CoinColor color, int column); //implements move for a player using his coin color.
	bool isWinner(CoinColor color) const; //check if player with given coin color made winning move. 

 private:
	int rows;
	int cols;
	int **grid; //grid holds the coins in board

	int *filled_rows; //describes the rows of a column filled with coins
	
	int played_coins; //total coins on the board so far

	int winning_moves; 
};

#endif
