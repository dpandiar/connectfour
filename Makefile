CC = g++
CFLAGS= -Wall

SOURCES = Board.cpp Player.cpp 
EXECUTABLE = ConnectFour
TEST = test_game

OBJS = ${SOURCES:.cpp=.o}

all: ${SOURCES} ${EXECUTABLE} ${TEST}

${EXECUTABLE}: ${EXECUTABLE}.cpp ${OBJS}
	${CC} $< ${OBJS} -o $@ 

${TEST}: ${TEST}.cpp ${OBJS}
	${CC} $< ${OBJS} -o $@ 

%.o: %.cpp
	${CC} $< ${CFLAGS} -c -o $@

clean:
	rm ${TEST} ${EXECUTABLE} *.o 
