#include <iostream>
#include <cstring>

#include "Board.h"

using std::cout;
using std::endl;

Board::Board(int num_rows, int num_cols, int num_win_moves) {
	rows = num_rows;
	cols = num_cols;
	winning_moves = num_win_moves;

	//allocate memory for grid that holds the coins in board
	grid = new int*[rows];
	for(int i = 0; i < rows; i++) {
		grid[i] = new int[cols]; 
		for(int j = 0; j < cols; j++) {
			grid[i][j] = 0;
		}
	}

	//allocate memory and intialize filled_rows to 0.
	filled_rows = new int[cols];
	std::memset(filled_rows, 0, cols*sizeof(int));

	played_coins = 0;
}

Board::~Board() {
	for(int i = 0; i < rows; i++) {
		delete [] grid[i]; 
	}
	delete [] grid;
	
	delete [] filled_rows;
}

void Board::draw() const {

	//upper horizontal boundary
	for (int j = 0; j < 2*cols; j++) {
		cout << "-";
	}	
	cout << endl;

	//draw the grid with coins
	for(int i = rows-1; i >= 0; i--) {
		for(int j = 0; j < cols; j++) {
			cout << "|";
			
			if(grid[i][j] == RED) {
				cout << "R";
			} else if(grid[i][j] == YELLOW) {
				cout << "Y";
			} else {
				cout << " ";
			}
		}
		cout << "|" << endl;
	}
	
	//lower horizontal boundary
	for (int j = 0; j < 2*cols; j++) {
		cout << "-";
	}
	cout << endl << endl;
}

bool Board::isFull() const {
	if(played_coins == rows*cols) {
		return true;
	} 
	return false;
}

bool Board::isColFull(int col) const {
	if(filled_rows[col] >= rows) {
		return true;
	}
	return false;
}

void Board::make_move(CoinColor color, int col) {
	//vacant row for column is right above its current filled row.
	int next_vacant_row = filled_rows[col];	
	if(next_vacant_row < rows) {
		grid[next_vacant_row][col] = color;
		filled_rows[col]++;	
		played_coins++;
	}	
}

/* This function iterates over the entire board checking for consecutive rows,
 * columns, diagonals (left and right) with same coin color. So calling this
 * function is expensive. 
 */
bool Board::isWinner(CoinColor color) const {
	int consecutive_coins = 0;

	//check if there are consecutive rows of this color for a winning move.
	for(int j = 0; j < cols; j++) {
		consecutive_coins = 0;
		for(int i = 0; i < rows; i++) {
			if(grid[i][j] == color) {
				consecutive_coins++;
			} else {
				consecutive_coins = 0;
			}

			if(consecutive_coins == winning_moves) {
				cout << winning_moves << " consecutive rows!" << endl;	
				return true;
			}
		}
	}

	//check if there are consecutive columns of this color for a winning move.
	for(int i = 0; i < rows; i++) {
		consecutive_coins = 0;
		for(int j = 0; j < cols; j++) {
			if(grid[i][j] == color) {
				consecutive_coins++;
			} else {
				consecutive_coins = 0;
			}

			if(consecutive_coins == winning_moves) {
				cout << winning_moves << " consecutive columns!" << endl;	
				return true;
			}
		}
	}
	
	//check if there are consecutive right diagonals of this color for a winning move.
	for(int i = 0; i < rows; i++) {
		for(int j = 0; j <= cols/2; j++) {
			consecutive_coins = 0;
			for(int k = 0; k < winning_moves; k++) {
				if(i+k < rows && j+k < cols) {
					if(grid[i+k][j+k] == color) {
						consecutive_coins++;
					} else {
						consecutive_coins = 0;
					}
					if(consecutive_coins == winning_moves) {
						cout << winning_moves << " consecutive right diagonals!" << endl;	
						return true;
					}
				}
			}	
		}
	}
	
	//check if there are consecutive left diagonals of this color for a winning move.
	for(int i = 0; i < rows; i++) {
		for(int j = cols/2; j < cols; j++) {
			consecutive_coins = 0;
			for(int k = 0; k < winning_moves; k++) {
				if(i+k < rows && j-k >= 0) {
					if(grid[i+k][j-k] == color) {
						consecutive_coins++;
					} else {
						consecutive_coins = 0;
					}
					if(consecutive_coins == winning_moves) {
						cout << winning_moves << " consecutive left diagonals!" << endl;	
						return true;
					}
				}	
			}	
		}
	}
	
	return false;
}
