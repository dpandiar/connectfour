#ifndef PLAYER_H
#define PLAYER_H

#include "Board.h"

class Player {
	
 public:
	Player(Board*, CoinColor);

	void move(int column);
	CoinColor get_coincolor();

 private:
	Board* board; //game board a player is playing at
	CoinColor coincolor; //coin color of player
};

#endif
