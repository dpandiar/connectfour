#include <iostream>
#include "Player.h"

using std::cout;
using std::endl;

Player::Player(Board* gameboard, CoinColor color) {
	board = gameboard;
	coincolor = color; 
}

CoinColor Player::get_coincolor() {
	return coincolor;
}

void Player::move(int column) {
	board->make_move(coincolor, column);
}
